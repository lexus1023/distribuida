package uce.edu.bautista;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientSpringThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientSpringThymeleafApplication.class, args);
	}
}

package uce.edu.bautista.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uce.edu.bautista.model.Instrument;

/**
 * Created by Alexis on 02/03/2018.
 */
@Controller
public class InstrumentController {

    @GetMapping(value = "/instrument/create")
    public String createInstrument(Model model){
        model.addAttribute("instrument",new Instrument());
        return "/form";
    }
}

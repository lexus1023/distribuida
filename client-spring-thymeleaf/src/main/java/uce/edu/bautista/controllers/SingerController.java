package uce.edu.bautista.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import uce.edu.bautista.model.Singer;

/**
 * Created by Alexis on 02/03/2018.
 */
@RestController
public class SingerController {

    @RequestMapping(value = "/singer/create")
    public String createSinger(Model model){
        model.addAttribute("singer",new Singer());
        return "/form2";
    }
}

package uce.edu.bautista.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uce.edu.bautista.model.Singer;
import uce.edu.bautista.services.SingerService;

/**
 * Created by Alexis on 02/03/2018.
 */
@Controller
public class SingerController {

    @Autowired
    SingerService singerService;


    @RequestMapping(value = "/singer/create")
    public String createSinger(Model model){
        model.addAttribute("singer",new Singer());
        return "/form";
    }


    @RequestMapping(value = "/singer/listar")
    public String listSingers(Model model){
        model.addAttribute("singers",singerService.listSinger());
        return "/list";
    }


    @RequestMapping(value = "/singer/save",method = RequestMethod.POST)
    public String saveSinger(Singer singer){
        Singer pers=singerService.saveSinger(singer);
        System.out.println(pers);
        return "redirect:/";
    }


}

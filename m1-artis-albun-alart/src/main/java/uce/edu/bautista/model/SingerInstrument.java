package uce.edu.bautista.model;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Alexis on 02/03/2018.
 */

@Entity
@Table(name="singinstr")
public class SingerInstrument implements Serializable {

    @Id
    @Column(name = "singerId")
    private Integer singerId;
    @Id
    @Column(name = "instrumentId")
    private Integer instrumentId;
}

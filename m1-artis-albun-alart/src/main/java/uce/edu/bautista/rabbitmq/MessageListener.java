package uce.edu.bautista.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uce.edu.bautista.M1ArtisAlbunAlartApplication;
import org.springframework.amqp.core.Message;
import uce.edu.bautista.model.Singer;
import uce.edu.bautista.services.SendMailService;
import uce.edu.bautista.services.SingerService;

import java.util.List;

/**
 * Created by Alexis on 02/03/2018.
 */
@Service
public class MessageListener {
    private static final Logger log = LoggerFactory.getLogger(MessageListener.class);

    @Autowired
    SendMailService sendMailService;
    @Autowired
    SingerService singerService;

    @RabbitListener(queues = M1ArtisAlbunAlartApplication.QUEUE_GENERIC_NAME)
    public void receiveMessage(final Message message) {
        log.info("Received message as generic: {}", message.toString());
    }

    @RabbitListener(queues = M1ArtisAlbunAlartApplication.QUEUE_SPECIFIC_NAME)
    public void receiveMessage(final uce.edu.bautista.rabbitmq.Message customMessage) {
        log.info("Received message as specific class: {}", customMessage.toString());

        //mandamos el mail y tenemos todo, con cada artista
//        sendMailService.sendMail("galegale1024@gmail.com","alexis23210@gmail.com");

    }
}

package uce.edu.bautista.services;

import uce.edu.bautista.model.Singer;

import java.util.List;

/**
 * Created by Alexis on 02/03/2018.
 */
public interface SingerService {
    Singer saveSinger(Singer singer);
    List<Singer> listSinger();
    Singer getSinger(Integer id);
}

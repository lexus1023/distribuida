package uce.edu.bautista.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uce.edu.bautista.model.Instrument;
import uce.edu.bautista.services.InstrumentService;

/**
 * Created by Alexis on 02/03/2018.
 */
@Controller
public class InstrumentController {

    @Autowired
    InstrumentService instrumentService;


    @RequestMapping(value = "/instrument/create")
    public String createInstrument(Model model){
        model.addAttribute("instrument",new Instrument());
        return "/form";
    }


    @RequestMapping(value = "/instrument/listar")
    public String listInstrument(Model model){
        model.addAttribute("instruments",instrumentService.listSinger());
        return "/list";
    }


    @RequestMapping(value = "/instrument/save",method = RequestMethod.POST)
    public String saveInstrument(Instrument singer){
        Instrument pers=instrumentService.saveSinger(singer);
        System.out.println(pers);
        return "redirect:/";
    }
}

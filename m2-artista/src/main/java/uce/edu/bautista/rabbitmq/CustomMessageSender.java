package uce.edu.bautista.rabbitmq;

import org.springframework.stereotype.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import uce.edu.bautista.M2ArtistaApplication;

/**
 * Created by Alexis on 02/03/2018.
 */
@Service
public class CustomMessageSender {
    private static final Logger log = LoggerFactory.getLogger(CustomMessageSender.class);

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public CustomMessageSender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage() {
        final Message message = new Message("Hello there!");
        log.info("Sending message...");
        rabbitTemplate.convertAndSend(M2ArtistaApplication.EXCHANGE_NAME, M2ArtistaApplication.ROUTING_KEY, message);
    }
}

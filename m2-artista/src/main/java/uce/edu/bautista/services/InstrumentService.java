package uce.edu.bautista.services;

import uce.edu.bautista.model.Instrument;

import java.util.List;

/**
 * Created by Alexis on 02/03/2018.
 */
public interface InstrumentService {
    Instrument saveSinger(Instrument singer);
    List<Instrument> listSinger();
    Instrument getSinger(Integer id);
}
